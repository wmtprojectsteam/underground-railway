#!/usr/bin/env python3

import RPi.GPIO as GPIO

# Define Train Position Detection Pins
sensor_a = 5                            # Sensor A (Store End) Board Pin 29 
sensor_b = 6                            # Sensor B (LHS of Centre Section) Board Pin 31
sensor_c = 7                            # Sensor C (RHS of Centre Section) Board Pin 26
sensor_d = 8                            # Sensor D (Main Door End) Board Pin 24

# Initialise and setup starting conditions
GPIO.setmode(GPIO.BCM)                  # Numbers GPIOs by Broadcom Pin Numbers
GPIO.setup(sensor_a, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin
GPIO.setup(sensor_b, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin
GPIO.setup(sensor_c, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin
GPIO.setup(sensor_d, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin

def sensor_a_detected():
    print("")
    print("Sensor A Detected")
    print("")
    
def sensor_b_detected():
    print("")
    print("Sensor B Detected")
    print("")
    
def sensor_c_detected():
    print("")
    print("Sensor C Detected")
    print("")
    
def sensor_d_detected():
    print("")
    print("Sensor D Detected")
    print("")
    
      
def destroy():
    GPIO.cleanup()                                              # Release resource

if __name__ == '__main__':                                      # Program start from here
    try:
        while True:
            print("Activate Sensor A")
            GPIO.wait_for_edge(sensor_a, GPIO.RISING)
            sensor_a_detected()

            print("Activate Sensor B")
            GPIO.wait_for_edge(sensor_b, GPIO.RISING)
            sensor_b_detected()

            print("Activate Sensor C")
            GPIO.wait_for_edge(sensor_c, GPIO.RISING)
            sensor_c_detected()

            print("Activate Sensor D")
            GPIO.wait_for_edge(sensor_d, GPIO.RISING)
            sensor_d_detected()

            print("End of cycle")
            print("")
            

    except KeyboardInterrupt:                                   # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
