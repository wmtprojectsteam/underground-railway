#!/usr/bin/env python3

import RPi.GPIO as GPIO

# Define Traffic Signals Pins
back_red = 22                           # Red Light just before Central Stop going Back  
back_green = 23                         # Green Light just before Central Stop going Back
out_red = 24                            # Red Light just before Central Stop going Out
out_green = 25                          # Green Light just before Central Stop going Out

# Initialise and setup starting conditions
GPIO.setmode(GPIO.BCM)                  # Numbers GPIOs by Broadcom Pin Numbers
GPIO.setup(back_red, GPIO.OUT)          # LED control pin
GPIO.setup(back_green, GPIO.OUT)        # ditto
GPIO.setup(out_red, GPIO.OUT)           # LED control pin
GPIO.setup(out_green, GPIO.OUT)         # ditto

GPIO.output(back_green, GPIO.LOW)      # Set all lights off
GPIO.output(out_green, GPIO.LOW)
GPIO.output(back_red, GPIO.LOW)
GPIO.output(out_red, GPIO.LOW)

print("All LEDs OFF")
input("Press Enter")

def led_cycle():
    GPIO.output(out_red, GPIO.LOW)          # Set Central Out Lights to Green
    GPIO.output(out_green, GPIO.HIGH)
    print("Out Green On.  Out Red Off.")
    print("")
    input("Press Enter")

    GPIO.output(back_green, GPIO.LOW)       # Set all lights off
    GPIO.output(out_green, GPIO.LOW)
    GPIO.output(back_red, GPIO.LOW)
    GPIO.output(out_red, GPIO.LOW)
    print("All Off.")
    print("")
    input("Press Enter")

    GPIO.output(out_red, GPIO.HIGH)         # Set Central Out Lights to Red
    GPIO.output(out_green, GPIO.LOW)
    print("Out Red On.  Out Green Off.")
    print("")
    input("Press Enter")

    GPIO.output(back_green, GPIO.LOW)       # Set all lights off
    GPIO.output(out_green, GPIO.LOW)
    GPIO.output(back_red, GPIO.LOW)
    GPIO.output(out_red, GPIO.LOW)
    print("All Off.")
    print("")
    input("Press Enter")

    GPIO.output(back_red, GPIO.LOW)         # Set Central Back Lights to Green
    GPIO.output(back_green, GPIO.HIGH)
    print("Back Green On.  Back Red Off.")
    print("")
    input("Press Enter")

    GPIO.output(back_green, GPIO.LOW)       # Set all lights off
    GPIO.output(out_green, GPIO.LOW)
    GPIO.output(back_red, GPIO.LOW)
    GPIO.output(out_red, GPIO.LOW)
    print("All Off.")
    print("")
    input("Press Enter")

    GPIO.output(back_red, GPIO.HIGH)        # Set Central Back Lights to Red
    GPIO.output(back_green, GPIO.LOW)
    print("Out Red On.  Out Green Off.")
    print("")
    input("Press Enter")

    GPIO.output(back_green, GPIO.LOW)       # Set all lights off
    GPIO.output(out_green, GPIO.LOW)
    GPIO.output(back_red, GPIO.LOW)
    GPIO.output(out_red, GPIO.LOW)
    print("All Off.")
    print("")
    input("Press Enter")
    print("")
    print("End of cycle")


def destroy():
    GPIO.cleanup()                                              # Release resource

if __name__ == '__main__':                                      # Program start from here
    try:
        while True:
            led_cycle()

    except KeyboardInterrupt:                                   # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
