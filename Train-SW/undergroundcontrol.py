#!/usr/bin/env python3
# Underground_Railway_Control.py:
#                Wimborne Model Town
#      Underground Railway System Control Functions
#
#  **********************************************************************
# This program carries out the following functions:
#   With reference to this diagram:
#
#        Out -->                                               <-- Back 
#
#   Store End                   Centre Section              Main Door End
#   ==  =============================================================  ==
#      ^       ^             ^        ^        ^            ^        ^
#      |       |             |        |        |            |        |
#    Track  Sensor        Sensor   Traffic   Sensor       Sensor   Track
#    Break     A             B     Signals      C            D     Break
#
#   1. On the press of a button on the guard-rail:
#
#   2. The train starts at the end nearest the Railway Room Store and picks up 
#      speed in the Out direction taking approximately 2 seconds to reach full
#      speed.
#
#   3. The train then maintains a set speed down the track until it passes Sensor
#      B, when it reduces speed over approximately 2 seconds and then stops in the
#      centre section where there will already be a red traffic signal.
#
#   4. After approximately 5 seconds, the traffic signal changes to green and the
#      train continues its journey down the track, accelerating to full speed over
#      approximately 2 seconds.
#
#   5. At sensor D, the train slows over approximately 2 seconds and stops when
#      the Track Break is passed at the Main Door End.
#
#   6. The train waits at the Main Door End for approximately 5 seconds.
#
#   7. The sequence is repeated in the Back direction decelerating at Sensors C and A
#      and stopping for approximately 5 seconds at the Store End.
#
#   8. Steps 2 to 7 are repeated for a specified number of loops and then the train
#      stops at the Store end until the guard rail button is pressed again.
#
# Copyright (c) 2019 Wimborne Model Town http://www.wimborne-modeltown.com/
#
#    This code is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Scheduler.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    with this software.  If not, see <http://www.gnu.org/licenses/>.
#
#  ***********************************************************************

import RPi.GPIO as GPIO
import time

# Define Train Motor Control Pins
out_direction   = 17                    # Motor Board Pin IN1 Board Pin 11
back_direction   = 27                   # Motor Board Pin IN2 Board Pin 13
pwm = 18                                # PWM pin EnB Board Pin 12

# Define Train Position Detection Pins
sensor_a = 5                            # Sensor A (Store End) Board Pin 29
sensor_b = 6                            # Sensor B (LHS of Centre Section) Board Pin 31
sensor_c = 7                            # Sensor C (RHS of Centre Section) Board Pin 26
sensor_d = 8                            # Sensor D (Main Door End) Board Pin 24

# Define Traffic Signals Pins
back_red = 22                           # Red Light just before Central Stop going Back
back_green = 23                         # Green Light just before Central Stop going Back
out_red = 24                            # Red Light just before Central Stop going Out
out_green = 25                          # Green Light just before Central Stop going Out

# Define Visitor Present Pin
button_pressed = 9                      # Visitor has pressed the button on the Guard Rail
                                        # Board Pin 21

# Define other variables (speed, wait times etc)
max_speed = 100                         # Maximum Speed in percentage PWM (just over 10 V)
min_speed = 65                          # Minimum Speed in percentage PWM (just under 8 V)
stop_speed = 0                          # Speed to stop the train.
rate_of_change = 1                      # Size of PWM increments.
wait_time = 5                           # Define wait time at stations in seconds
accel_time = 2                          # Define time to accelerate / decelerate train in seconds
pause = 1                               # Pause after lights change
direction = "Out"                       # Initialise to start in the Out direction

#  Calculate how many times to continue checking if the has passed a Sensor.
sensor_pause_time = 0.5                # Time in seconds for the program to pause each time round the Sensor test loop.

# Calculate number of loops in out leg.
sensor_test_time_out = 7                # Time in seconds required.
sensor_test_loops_out = (sensor_test_time_out / sensor_pause_time)
print(sensor_test_loops_out)

# Calculate number of loops in back leg.
sensor_test_time_back = 6               # Time in seconds required.
sensor_test_loops_back = (sensor_test_time_back / sensor_pause_time)
print(sensor_test_loops_back)

# Overall control
num_loops_button_press = 1              # Set this to define how many times the train goes Out and Back after Button pressed
num_loops_rewarm = 1                    # Set this to define how many times the train goes Out and Back after a warmup cycle is triggered
cool_time = 60                          # Time in Minutes before a warm cycle commences.


# Calculate how long to delay each loop iteration in the train_start() / train_stop () functions to obtained the required acceleration time
loop_delay = accel_time/((max_speed - min_speed)/rate_of_change)

# Initialise and setup starting conditions
GPIO.setmode(GPIO.BCM)                  # Numbers GPIOs by Broadcom Pin Numbers
GPIO.setup(out_direction, GPIO.OUT)     # Control pins for Motor Drive Board
GPIO.setup(back_direction, GPIO.OUT)    # ditto
GPIO.setup(pwm, GPIO.OUT)               # ditto

GPIO.setup(back_red, GPIO.OUT)          # LED control pin
GPIO.setup(back_green, GPIO.OUT)        # ditto
GPIO.setup(out_red, GPIO.OUT)           # LED control pin
GPIO.setup(out_green, GPIO.OUT)         # ditto


GPIO.setup(sensor_a, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin
GPIO.setup(sensor_b, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin
GPIO.setup(sensor_c, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin
GPIO.setup(sensor_d, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)           # Sensor pin

GPIO.setup(button_pressed, GPIO.IN, pull_up_down=GPIO.PUD_UP)     # Button pin

PWM = GPIO.PWM(pwm, 1000)               # Create object PWM on pwm port at 1000 Hz
PWM.start(0)                            # Start motor on 0 percent duty cycle (off)

GPIO.output(out_direction, GPIO.LOW)    # reset both motor pins
GPIO.output(back_direction, GPIO.LOW)

GPIO.output(back_green, GPIO.LOW)      # Set all lights to red
GPIO.output(out_green, GPIO.LOW)
GPIO.output(back_red, GPIO.HIGH)
GPIO.output(out_red, GPIO.HIGH)

def train_start():
    global direction
    # Accellerate train in the required direction
    speed = min_speed
    if direction == "Out":
        print("Train Start - Out")
        while speed <= max_speed:
            GPIO.output(out_direction, GPIO.HIGH)           # train down the track
            GPIO.output(back_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed + rate_of_change
            time.sleep(loop_delay)
        PWM.ChangeDutyCycle(max_speed)                      # Allows for intermediate values of PWM.
    elif direction == "Back":
        print("Train Start - Back")
        while speed <= max_speed:
            GPIO.output(back_direction, GPIO.HIGH)          # train up the track
            GPIO.output(out_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed + rate_of_change
            time.sleep(loop_delay)
        PWM.ChangeDutyCycle(max_speed)                      # Allows for intermediate values of PWM.


def train_stop():
    global direction
    # Calculate how long to delay each loop iteration to obtained the required deceleration time
    decel_loop_delay = accel_time/((max_speed - stop_speed)/rate_of_change)

    # Decellerate train to a stop
    speed = max_speed
    if direction == "Out":
        print("Train Stop - Out")
        while speed > stop_speed:
            GPIO.output(out_direction, GPIO.HIGH)          # train down the track
            GPIO.output(back_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed - rate_of_change
            time.sleep(decel_loop_delay)
        PWM.ChangeDutyCycle(stop_speed)                      # Allows for intermediate values of PWM.
    elif direction == "Back":
        print("Train Stop - Back")
        while speed > stop_speed:
            GPIO.output(back_direction, GPIO.HIGH)          # train up the track
            GPIO.output(out_direction, GPIO.LOW)
            PWM.ChangeDutyCycle(speed)
            speed = speed - rate_of_change
            time.sleep(decel_loop_delay)
        PWM.ChangeDutyCycle(stop_speed)                      # Allows for intermediate values of PWM.

def run_cycle(num_loops):
    global direction, sensor_test_loops_out, sensor_test_loops_back

    loop_count = 0

    # Start at Store End and run "Out"
    while loop_count < num_loops:
        # Starting at the Store End Travel "Out" towards the Tunnel
        direction = "Out"

        GPIO.output(out_red, GPIO.LOW)                  # Set Central Out Lights to Green
        GPIO.output(out_green, GPIO.HIGH)
        time.sleep(pause)                               # Pause a short time after the lights change

        print("First Leg Out.  Waiting for Sensor B after speed reaches max.")

        train_start()                                   # Train will start and accellerate towards the Tunnel.

        i = sensor_test_loops_out                       # Set the loop counter to give the required time testing for the sensor

        while i >= 0:
            if not GPIO.input(sensor_b):
                time.sleep(0.25)
            else:
                print ("Sensor B Activated")
                break

        train_stop()                                    # Train will decellerate and stop at the central Halt.

        time.sleep(wait_time)                           #Train will wait in the Tunnel.

        GPIO.output(out_red, GPIO.HIGH)                 # Set Central Out Lights to Red
        GPIO.output(out_green, GPIO.LOW)

        time.sleep(pause)                               # Pause a short time after the lights change

        print("Second Leg Out.  Waiting for Sensor D after speed reaches max")

        train_start()                                   # Train will start and accellerate towards the Main Door end.

        i = sensor_test_loops_out                       # Set the loop counter to give the required time testing for the sensor

        while i >= 0:
            if not GPIO.input(sensor_d):
                i -= 1
                time.sleep(0.25)
            else:
                print ("Sensor D Activated")
                break

        train_stop()

        time.sleep(wait_time)                           # Train will wait at the Main Door End.

        # Starting at the Main Door End Travel "Back" towards the Tunnel
        direction = "Back"

        GPIO.output(back_red, GPIO.LOW)                # Set Central Back Lights to Green
        GPIO.output(back_green, GPIO.HIGH)

        time.sleep(pause)                               # Pause a short time after the lights change

        print("First Leg Back.  Waiting for Sensor C after speed reaches max.")

        train_start()                                   # Train will start, accellerate, decellerate, stop before return

        i = sensor_test_loops_back                      # Set the loop counter to give the required time testing for the sensor

        while i >= 0:
            if not GPIO.input(sensor_c):
                i -= 1
                time.sleep(0.25)
            else:
                print ("Sensor C Activated")
                break

        train_stop()

        time.sleep(wait_time)                           # Train will wait in the Tunnel.

        GPIO.output(back_red, GPIO.HIGH)                 # Set Central Back Lights to Red
        GPIO.output(back_green, GPIO.LOW)

        time.sleep(pause)                               # Pause a short time after the lights change

        print("Second Leg Back.  Waiting for Sensor A after speed reaches max.")

        train_start()                                   # Train will start, accellerate, decellerate, stop before return

        i = sensor_test_loops_back                      # Set the loop counter to give the required time testing for the sensor

        while i >= 0:
            if not GPIO.input(sensor_a):
                i -= 1
                time.sleep(0.25)
            else:
                print ("Sensor A Activated")
                break

        train_stop()

        time.sleep(wait_time)                           # Train will wait at the store end.

        loop_count += 1

    PWM.ChangeDutyCycle(0)                              # Remove power from the train until the next time.


def destroy():
    GPIO.cleanup()                                              # Release resource

if __name__ == '__main__':                                      # Program start from here
    try:
        print("Press the Guardrail Button")
        while True:
            # Wait for button to be pressed
            if GPIO.input(button_pressed):                      # Line pulled high if button not pressed; go round again.
                    time.sleep(0.25)                            # Keep waiting until Button pressed.
            else:
                print ("Button Pressed")
                run_cycle(num_loops_button_press)               # Button press detected, do normal cycle
                print("Press the Guardrail Button")

    except KeyboardInterrupt:                                   # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        destroy()
